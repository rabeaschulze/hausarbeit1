class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :name
      t.integer :studentnumber
      t.string :institute
      t.integer :preference_value

      t.timestamps
    end
  end
end
