class AddDetailsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :birthday, :date
    add_column :users, :study_programme, :string
    add_column :users, :semester, :integer
    add_column :users, :head_of_institute, :string
    add_column :users, :research_fields, :string
  end
end
