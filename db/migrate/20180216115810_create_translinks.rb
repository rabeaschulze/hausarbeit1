class CreateTranslinks < ActiveRecord::Migration[5.1]
  def change
    create_table :translinks do |t|
      t.string :student_id
      t.string :institute_id
      t.integer :preference_value
      t.binary :matching

      t.timestamps
    end
  end
end
