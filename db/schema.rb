# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180305174113) do

  create_table "assignments", force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_assignments_on_role_id"
    t.index ["user_id"], name: "index_assignments_on_user_id"
  end

  create_table "gams_paths", force: :cascade do |t|
    t.string "gams_path_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "institutes", force: :cascade do |t|
    t.string "name"
    t.integer "number_of_professors"
    t.integer "number_of_employees"
    t.float "excess_capacity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "description"
  end

  create_table "students", force: :cascade do |t|
    t.string "name"
    t.integer "studentnumber"
    t.string "institute"
    t.integer "preference_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "institute_id"
  end

  create_table "translinks", force: :cascade do |t|
    t.string "student_id"
    t.string "institute_id"
    t.integer "preference_value"
    t.binary "matching"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.boolean "admin", default: false
    t.date "birthday"
    t.string "study_programme"
    t.integer "semester"
    t.string "head_of_institute"
    t.string "research_fields"
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
