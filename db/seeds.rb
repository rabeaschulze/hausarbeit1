# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# User-Roles
Role.create!([
              { name: 'Admin', description: 'Admin'},
              { name: 'Student', description: 'Student'},
              { name: 'Institute', description: 'Institute'}
            ])

# Admin-Accounts

User.create!(name:  "Admin",
             email: "admin@matching-app.org",
             password:              "matching",
             password_confirmation: "matching",
             admin: true
             )

User.create!(name:  "Stefan Helber",
            email: "stefan.helber@prod.uni-hannover.de",
            password:              "geheim",
            password_confirmation: "geheim",
            admin: true
            )

# Assignment to admins
Assignment.create!(user_id: 1, role_id: 1)
Assignment.create!(user_id: 2, role_id: 1)

# Student Accounts
User.create!(name: "Emelie Dümpelmann",
            email: "e.duempelm@web.de",
            password: "matching",
            password_confirmation: "matching",
            )

User.create!(name: "Rabea Schulze",
            email: "rabea-schulze@web.de",
            password: "matching",
            password_confirmation: "matching",
            birthday: "20.09.1994",
            study_programme: "business sciences",
            semester: "3",
            )

User.create!(name: "Donald Trump",
            email: "donald-trump@ovaloffice.com",
            password: "AmericaFirst",
            password_confirmation: "AmericaFirst",
            )

User.create!(name: "Angela Merkel",
            email: "angela.merkel@bundesregierung.de",
            password: "bundeskanzlerin",
            password_confirmation: "bundeskanzlerin",
            )

User.create!(name: "Donald Duck",
            email: "donalduck@entenhausen.org",
            password: "daisyduck",
            password_confirmation: "daisyduck",
            )

User.create!(name: "Wolfgang Petry",
            email: "wolfgangpetry@schlagermusik.org",
            password: "schlagerforever",
            password_confirmation: "schlagerforever",
            )

User.create!(name: "Udo Jürgens",
            email: "udojuergens@schlagermusik.org",
            password: "newyork",
            password_confirmation: "newyork",
            )

User.create!(name: "Helene Fischer",
            email: "helenefischer@schlagermusik.org",
            password: "popschlager",
            password_confirmation: "popschlager",
            )

User.create!(name: "Stephan Weil",
            email: "s.weil@niedersachen.de",
            password: "landesministerium",
            password_confirmation: "landesministerium",
            )

User.create!(name: "Harry Potter",
            email: "hpotter@rowling.com",
            password: "SteinDerWeisen",
            password_confirmation: "SteinDerWeisen",
            admin: false)

User.create!(name: "Hermine Granger",
            email: "hgranger@rowling.com",
            password: "GefangeneVonAskaban",
            password_confirmation: "GefangeneVonAskaban",
            )

User.create!(name: "Ron Weasley",
            email: "rweasley@rowling.com",
            password: "Halbblutprinz",
            password_confirmation: "Halbblutprinz",
            )

User.create!(name: "Stefanie Helber",
            email: "stefanie-helber@familie.org",
            password: "Bruder",
            password_confirmation: "Bruder",
            )

User.create!(name: "Dan Brown",
            email: "dan.brown@verleger.org",
            password: "Thriller",
            password_confirmation: "Thriller",
            )

User.create!(name: "Hildegard Knef",
            email: "h.knef@schauspiel.org",
            password: "Theater",
            password_confirmation: "Theater",
            )

User.create!(name: "Benny Banane",
            email: "banane@obst.org",
            password: "GelbeFrucht",
            password_confirmation: "GelbeFrucht",
            )

User.create!(name: "Emily Erdbeere",
            email: "erdbeere@obst.org",
            password: "RoteFrucht",
            password_confirmation: "RoteFrucht",
            )

User.create!(name: "Frank Feige",
            email: "feige@obst.org",
            password: "LilaFrucht",
            password_confirmation: "LilaFrucht",
            )

User.create!(name: "Paula Pflaume",
            email: "pflaume@obst.org",
            password: "VioletteFrucht",
            password_confirmation: "VioletteFrucht",
            )

User.create!(name: "Ingo Appelt",
            email: "i.appelt@comedian.org",
            password: "comedy",
            password_confirmation: "comedy",
            )

User.create!(name: "Ted Mosby",
            email: "t.mosby@himym.com",
            password: "blackhair",
            password_confirmation: "blackhair",
            )

User.create!(name: "Marshall Eriksen",
            email: "m.eriksen@himym.com",
            password: "ilovelily",
            password_confirmation: "ilovelily",
            )

User.create!(name: "Lily Aldrin",
            email: "l.aldrin@himym.com",
            password: "redhair",
            password_confirmation: "redhair",
            )

User.create!(name: "Barney Stinson",
            email: "b.stinson@himym.com",
            password: "playboy",
            password_confirmation: "playboy",
            )

User.create!(name: "Robin Scherbatsky",
            email: "r.scherbatsky@himym.com",
            password: "brownhair",
            password_confirmation: "brownhair",
            )

User.create!(name: "Rune Dahmke",
            email: "dahmke@handball.de",
            password: "linksaußen",
            password_confirmation: "linksaußen",
            )

User.create!(name: "Steffen Weinhold",
            email: "weinhold@handball.de",
            password: "rückraumrechts",
            password_confirmation: "rückraumrechts",
            )

User.create!(name: "Cristiano Ronaldo",
            email: "ronaldo@fussball.com",
            password: "halamadrid",
            password_confirmation: "halamadrid",
            )

User.create!(name: "Lionel Messi",
            email: "messi@fussball.com",
            password: "argentina",
            password_confirmation: "argentina",
            )

User.create!(name: "Lukas Podolski",
            email: "podolski@fussball.de",
            password: "kölschejung",
            password_confirmation: "kölschejung",
            )

User.create!(name: "Manuel Neuer",
            email: "neuer@fussball.de",
            password: "fcbayern",
            password_confirmation: "fcbayern",
            )

User.create!(name: "Thomas Müller",
            email: "mueller@fussball.de",
            password: "miasanmia",
            password_confirmation: "miasanmia",
            )
User.create!(name: "Dagur Sigurdsson",
            email: "sigurdsson@handball.de",
            password: "handballcoach",
            password_confirmation: "handballcoach",
            )

User.create!(name: "Sigmar Gabriel",
            email: "gabriel@bundesregierung.de",
            password: "wirtschaftsminister",
            password_confirmation: "wirtschaftsminister",
            )

User.create!(name: "Wladimir Putin",
            email: "putin@russland.de",
            password: "nastrovje",
            password_confirmation: "nastrovje",
            )

User.create!(name: "Kim-Jong Un",
            email: "un@korea.de",
            password: "nointernet",
            password_confirmation: "nointernet",
            )

User.create!(name: "Wolfgang Schäuble",
            email: "schaeuble@bundesregierung.de",
            password: "bundestag",
            password_confirmation: "bundestag",
            )

User.create!(name: "Chuck Norris",
            email: "norris@witze.com",
            password: "whoischucknorris",
            password_confirmation: "whoischucknorris",
            )

User.create!(name: "Uwe Gensheimer",
            email: "gensheimer@handball.de",
            password: "außenlinks",
            password_confirmation: "außenlinks",
            )

User.create!(name: "Steffen Fäth",
            email: "faeth@handball.de",
            password: "rückraumlinks",
            password_confirmation: "rückraumlinks",
            )

User.create!(name: "Finn Lemke",
            email: "lemke@handball.de",
            password: "rechtsaußen",
            password_confirmation: "rechtsaußen",
            )

User.create!(name: "Gregor Gysi",
            email: "gysi@bundesregierung.de",
            password: "dielinke",
            password_confirmation: "dielinke",
            )

User.create!(name: "Ulrike von der Leyen",
            email: "von-der-leyen@bundesregierung.de",
            password: "verteidigung",
            password_confirmation: "verteidigung",
            )

User.create!(name: "Joachim Gauck",
            email: "gauck@bundesregierung.de",
            password: "exbundespräsident",
            password_confirmation: "exbundespräsident",
            )

User.create!(name: "Walter Steinmeier",
            email: "steinmeier@bundesregierung.de",
            password: "bundespräsident",
            password_confirmation: "bundespräsident",
            )

User.create!(name: "Nicolas Sarkozy",
            email: "sarkozy@frankreich.fr",
            password: "expresidente",
            password_confirmation: "expresidente",
            )

User.create!(name: "Emmanuel Macron",
            email: "macron@frankreich.fr",
            password: "presidente",
            password_confirmation: "presidente",
            )

User.create!(name: "Minerva McGonagall",
            email: "mcgonagall@rowling.com",
            password: "kammerdesschreckens",
            password_confirmation: "kammerdesschreckens",
            )

User.create!(name: "Homer Simpson",
            email: "h.simpson@thesimpsons.com",
            password: "ilovemarge",
            password_confirmation: "ilovemarge",
            )

User.create!(name: "Karl Lagerfeld",
            email: "k.lagerfeld@mode.de",
            password: "choupette",
            password_confirmation: "choupette",
            )

# Assignments to Students
Assignment.create!(user_id: 3, role_id: 2)
Assignment.create!(user_id: 4, role_id: 2)
Assignment.create!(user_id: 5, role_id: 2)
Assignment.create!(user_id: 6, role_id: 2)
Assignment.create!(user_id: 7, role_id: 2)
Assignment.create!(user_id: 8, role_id: 2)
Assignment.create!(user_id: 9, role_id: 2)
Assignment.create!(user_id: 10, role_id: 2)
Assignment.create!(user_id: 11, role_id: 2)
Assignment.create!(user_id: 12, role_id: 2)
Assignment.create!(user_id: 13, role_id: 2)
Assignment.create!(user_id: 14, role_id: 2)
Assignment.create!(user_id: 15, role_id: 2)
Assignment.create!(user_id: 16, role_id: 2)
Assignment.create!(user_id: 17, role_id: 2)
Assignment.create!(user_id: 18, role_id: 2)
Assignment.create!(user_id: 19, role_id: 2)
Assignment.create!(user_id: 20, role_id: 2)
Assignment.create!(user_id: 21, role_id: 2)
Assignment.create!(user_id: 22, role_id: 2)
Assignment.create!(user_id: 23, role_id: 2)
Assignment.create!(user_id: 24, role_id: 2)
Assignment.create!(user_id: 25, role_id: 2)
Assignment.create!(user_id: 26, role_id: 2)
Assignment.create!(user_id: 27, role_id: 2)
Assignment.create!(user_id: 28, role_id: 2)
Assignment.create!(user_id: 29, role_id: 2)
Assignment.create!(user_id: 30, role_id: 2)
Assignment.create!(user_id: 31, role_id: 2)
Assignment.create!(user_id: 32, role_id: 2)
Assignment.create!(user_id: 33, role_id: 2)
Assignment.create!(user_id: 34, role_id: 2)
Assignment.create!(user_id: 35, role_id: 2)
Assignment.create!(user_id: 36, role_id: 2)
Assignment.create!(user_id: 37, role_id: 2)
Assignment.create!(user_id: 38, role_id: 2)
Assignment.create!(user_id: 39, role_id: 2)
Assignment.create!(user_id: 40, role_id: 2)
Assignment.create!(user_id: 41, role_id: 2)
Assignment.create!(user_id: 42, role_id: 2)
Assignment.create!(user_id: 43, role_id: 2)
Assignment.create!(user_id: 44, role_id: 2)
Assignment.create!(user_id: 45, role_id: 2)
Assignment.create!(user_id: 46, role_id: 2)
Assignment.create!(user_id: 47, role_id: 2)
Assignment.create!(user_id: 48, role_id: 2)
Assignment.create!(user_id: 49, role_id: 2)
Assignment.create!(user_id: 50, role_id: 2)
Assignment.create!(user_id: 51, role_id: 2)
Assignment.create!(user_id: 52, role_id: 2)

# Institute Accounts

User.create!(name: "Marketing",
            email: "marketing@uni-hannover.de",
            password: "sloganfan",
            password_confirmation: "sloganfan",
            )

User.create!(name: "Controlling",
            email: "controlling@uni-hannover.de",
            password: "checkeverything",
            password_confirmation: "checkeverything",
            )

User.create!(name: "Logistics",
            email: "logistics@uni-hannover.de",
            password: "TravellingSalesman",
            password_confirmation: "TravellingSalesman",
            )

User.create!(name: "Finance",
            email: "finance@uni-hannover.de",
            password: "welovenumbers",
            password_confirmation: "welovenumbers",
            )

User.create!(name: "Banking and Insurance",
            email: "banking-and-insurance@uni-hannover.de",
            password: "moneymoneymoney",
            password_confirmation: "moneymoneymoney",
            )

User.create!(name: "Business Taxation",
            email: "business-taxation@uni-hannover.de",
            password: "taxpayers",
            password_confirmation: "taxpayers",
            )

User.create!(name: "Business Information Systems",
            email: "business-information-systems@uni-hannover.de",
            password: "software",
            password_confirmation: "software",
            )

User.create!(name: "Microeconomics",
            email: "microeconomics@uni-hannover.de",
            password: "gametheory",
            password_confirmation: "gametheory",
            )
# Assignments to Institutes

Assignment.create!(user_id: 53, role_id: 3)
Assignment.create!(user_id: 54, role_id: 3)
Assignment.create!(user_id: 55, role_id: 3)
Assignment.create!(user_id: 56, role_id: 3)
Assignment.create!(user_id: 57, role_id: 2)
Assignment.create!(user_id: 58, role_id: 2)
Assignment.create!(user_id: 59, role_id: 2)
Assignment.create!(user_id: 60, role_id: 2)

# Institues

Inst1 = Institute.create!(name: "Marketing",
                          number_of_professors: 1,
                          number_of_employees: 8,
                          excess_capacity: 0.2)

Inst2 = Institute.create!(name: "Controlling",
                          number_of_professors: 1,
                          number_of_employees: 7,
                          excess_capacity: 0.3)

Inst3 = Institute.create!(name: "Logistics",
                          number_of_professors: 1,
                          number_of_employees: 9,
                          excess_capacity: 0.15)

Inst4 = Institute.create!(name: "Finance",
                          number_of_professors: 1,
                          number_of_employees: 3,
                          excess_capacity: 0.19)

Inst5 = Institute.create!(name: "Banking and Insurance",
                          number_of_professors: 1,
                          number_of_employees: 4,
                          excess_capacity: 0.24)

Inst6 = Institute.create!(name: "Business Taxation",
                          number_of_professors: 1,
                          number_of_employees: 6,
                          excess_capacity: 0.28)

Inst7 = Institute.create!(name: "Business Information Systems",
                          number_of_professors: 1,
                          number_of_employees: 4,
                          excess_capacity: 0.3)

Inst8 = Institute.create!(name: "Microeconomics",
                          number_of_professors: 1,
                          number_of_employees: 5,
                          excess_capacity: 0.1)

# Students

Stud1 = Student.create!(name: "Emelie Dümpelmann", studentnumber: 100, institute: Inst1, preference_value: 4)
Stud2 = Student.create!(name: "Rabea Schulze", studentnumber: 200, institute: Inst2, preference_value: 5)
Stud3 = Student.create!(name: "Donald Trump", studentnumber: 300, institute: Inst3, preference_value: 7)
Stud4 = Student.create!(name: "Angela Merkel", studentnumber: 400, institute: Inst4, preference_value: 4)
Stud5 = Student.create!(name: "Donald Duck", studentnumber: 500, institute: Inst2, preference_value: 5)
Stud6 = Student.create!(name: "Wolfgang Petry", studentnumber: 600, institute: Inst3, preference_value: 7)
Stud7 = Student.create!(name: "Udo Jürgens", studentnumber: 700, institute: Inst4, preference_value: 4)
Stud8 = Student.create!(name: "Helene Fischer", studentnumber: 800, institute: Inst2, preference_value: 5)
Stud9 = Student.create!(name: "Stephan Weil", studentnumber: 900, institute: Inst3, preference_value: 7)
Stud10 = Student.create!(name: "Harry Potter", studentnumber: 110, institute: Inst1, preference_value: 4)
Stud11 = Student.create!(name: "Hermine Granger", studentnumber: 210, institute: Inst2, preference_value: 5)
Stud12 = Student.create!(name: "Ron Weasley", studentnumber: 310, institute: Inst3, preference_value: 7)
Stud13 = Student.create!(name: "Stefanie Helber", studentnumber: 610, institute: Inst3, preference_value: 7)
Stud14 = Student.create!(name: "Dan Brown", studentnumber: 930, institute: Inst1, preference_value: 2)
Stud15 = Student.create!(name: "Hildegard Knef", studentnumber: 940, institute: Inst4, preference_value: 6)
Stud16 = Student.create!(name: "Benny Banane", studentnumber: 710, institute: Inst1, preference_value: 4)
Stud17 = Student.create!(name: "Emily Erdbeer", studentnumber: 810, institute: Inst4, preference_value: 5)
Stud18 = Student.create!(name: "Frank Feige", studentnumber: 910, institute: Inst3, preference_value: 7)
Stud19 = Student.create!(name: "Paula Pflaume", studentnumber: 120, institute: Inst2, preference_value: 5)
Stud20 = Student.create!(name: "Ingo Appelt", studentnumber: 220, institute: Inst3, preference_value: 7)
Stud21 = Student.create!(name: "Ted Mosby", studentnumber: 1234, institute: Inst1, preference_value: 4)
Stud22 = Student.create!(name: "Marshall Eriksen", studentnumber: 2345, institute: Inst2, preference_value: 5)
Stud23 = Student.create!(name: "Lily Aldrin", studentnumber: 3456, institute: Inst3, preference_value: 7)
Stud24 = Student.create!(name: "Barney Stinson", studentnumber: 4567, institute: Inst3, preference_value: 7)
Stud25 = Student.create!(name: "Robin Scherbatsky", studentnumber: 5678, institute: Inst1, preference_value: 2)
Stud26 = Student.create!(name: "Rune Dahmke", studentnumber: 6789, institute: Inst4, preference_value: 6)
Stud27 = Student.create!(name: "Steffen Winhold", studentnumber: 7891, institute: Inst1, preference_value: 4)
Stud28 = Student.create!(name: "Cristiano Ronaldo", studentnumber: 8912, institute: Inst4, preference_value: 5)
Stud29 = Student.create!(name: "Lionel Messi", studentnumber: 9123, institute: Inst3, preference_value: 7)
Stud30 = Student.create!(name: "Lukas Podolski", studentnumber: 1000, institute: Inst2, preference_value: 5)
Stud31 = Student.create!(name: "Manuel Neuer", studentnumber: 2000, institute: Inst3, preference_value: 7)
Stud32 = Student.create!(name: "Thomas Müller", studentnumber: 3000, institute: Inst1, preference_value: 4)
Stud33 = Student.create!(name: "Dagur Sigurdsson", studentnumber: 4000, institute: Inst2, preference_value: 5)
Stud34 = Student.create!(name: "Sigmar Gabriel", studentnumber: 5000, institute: Inst3, preference_value: 7)
Stud35 = Student.create!(name: "Vladimir Putin", studentnumber: 6000, institute: Inst3, preference_value: 7)
Stud36 = Student.create!(name: "Kim-Jong Un", studentnumber: 7000, institute: Inst1, preference_value: 2)
Stud37 = Student.create!(name: "Wolfgang Schäuble", studentnumber: 8000, institute: Inst4, preference_value: 6)
Stud38 = Student.create!(name: "Chuck Norris", studentnumber: 9000, institute: Inst1, preference_value: 4)
Stud39 = Student.create!(name: "Uwe Gensheimer", studentnumber: 1100, institute: Inst4, preference_value: 5)
Stud40 = Student.create!(name: "Steffen Fäth", studentnumber: 1200, institute: Inst3, preference_value: 7)
Stud41 = Student.create!(name: "Finn Lemke", studentnumber: 1300, institute: Inst2, preference_value: 5)
Stud42 = Student.create!(name: "Gregor Gysi", studentnumber: 1400, institute: Inst3, preference_value: 7)
Stud43 = Student.create!(name: "Ulrike von der Leyen", studentnumber: 1500, institute: Inst1, preference_value: 4)
Stud44 = Student.create!(name: "Joachim Gauck", studentnumber: 1600, institute: Inst2, preference_value: 5)
Stud45 = Student.create!(name: "Walter Steinmeier", studentnumber: 1700, institute: Inst3, preference_value: 7)
Stud46 = Student.create!(name: "Nicolas Sarkozy", studentnumber: 1800, institute: Inst1, preference_value: 2)
Stud47 = Student.create!(name: "Emmanuel Macron", studentnumber: 1900, institute: Inst4, preference_value: 6)
Stud48 = Student.create!(name: "Minerva McGonagall", studentnumber: 2100, institute: Inst1, preference_value: 4)
Stud49 = Student.create!(name: "Homer Simpson", studentnumber: 2200, institute: Inst4, preference_value: 5)
Stud50 = Student.create!(name: "Karl Lagerfeld", studentnumber: 2300, institute: Inst3, preference_value: 7)

TraLi1 = Translink.create!(student: Stud1, institute: Inst1, preference_value: 4, matching: 0)
TraLi2 = Translink.create!(student: Stud1, institute: Inst2, preference_value: 4, matching: 0)
TraLi3 = Translink.create!(student: Stud1, institute: Inst3, preference_value: 1, matching: 0)
TraLi4 = Translink.create!(student: Stud1, institute: Inst4, preference_value: 7, matching: 0)
TraLi5 = Translink.create!(student: Stud1, institute: Inst5, preference_value: 6, matching: 0)
TraLi6 = Translink.create!(student: Stud1, institute: Inst6, preference_value: 2, matching: 0)
TraLi7 = Translink.create!(student: Stud1, institute: Inst7, preference_value: 7, matching: 0)
TraLi8 = Translink.create!(student: Stud1, institute: Inst8, preference_value: 1, matching: 0)
TraLi9 = Translink.create!(student: Stud2, institute: Inst1, preference_value: 4, matching: 0)
TraLi10 = Translink.create!(student: Stud2, institute: Inst2, preference_value: 5, matching: 0)
TraLi11 = Translink.create!(student: Stud2, institute: Inst3, preference_value: 1, matching: 0)
TraLi12 = Translink.create!(student: Stud2, institute: Inst4, preference_value: 3, matching: 0)
TraLi13 = Translink.create!(student: Stud2, institute: Inst5, preference_value: 6, matching: 0)
TraLi14 = Translink.create!(student: Stud2, institute: Inst6, preference_value: 2, matching: 0)
TraLi15 = Translink.create!(student: Stud2, institute: Inst7, preference_value: 7, matching: 0)
TraLi16 = Translink.create!(student: Stud2, institute: Inst8, preference_value: 1, matching: 0)
TraLi17 = Translink.create!(student: Stud3, institute: Inst1, preference_value: 1, matching: 0)
TraLi18 = Translink.create!(student: Stud3, institute: Inst2, preference_value: 8, matching: 0)
TraLi19 = Translink.create!(student: Stud3, institute: Inst3, preference_value: 7, matching: 0)
TraLi20 = Translink.create!(student: Stud3, institute: Inst4, preference_value: 6, matching: 0)
TraLi21 = Translink.create!(student: Stud3, institute: Inst5, preference_value: 6, matching: 0)
TraLi22 = Translink.create!(student: Stud3, institute: Inst6, preference_value: 2, matching: 0)
TraLi23 = Translink.create!(student: Stud3, institute: Inst7, preference_value: 7, matching: 0)
TraLi24 = Translink.create!(student: Stud3, institute: Inst8, preference_value: 1, matching: 0)
TraLi25 = Translink.create!(student: Stud4, institute: Inst1, preference_value: 6, matching: 0)
TraLi26 = Translink.create!(student: Stud4, institute: Inst2, preference_value: 2, matching: 0)
TraLi27 = Translink.create!(student: Stud4, institute: Inst3, preference_value: 1, matching: 0)
TraLi28 = Translink.create!(student: Stud4, institute: Inst4, preference_value: 4, matching: 0)
TraLi29 = Translink.create!(student: Stud4, institute: Inst5, preference_value: 6, matching: 0)
TraLi30 = Translink.create!(student: Stud4, institute: Inst6, preference_value: 2, matching: 0)
TraLi31 = Translink.create!(student: Stud4, institute: Inst7, preference_value: 7, matching: 0)
TraLi32 = Translink.create!(student: Stud4, institute: Inst8, preference_value: 1, matching: 0)
TraLi33 = Translink.create!(student: Stud5, institute: Inst1, preference_value: 4, matching: 0)
TraLi34 = Translink.create!(student: Stud5, institute: Inst2, preference_value: 5, matching: 0)
TraLi35 = Translink.create!(student: Stud5, institute: Inst3, preference_value: 5, matching: 0)
TraLi36 = Translink.create!(student: Stud5, institute: Inst4, preference_value: 1, matching: 0)
TraLi37 = Translink.create!(student: Stud5, institute: Inst5, preference_value: 6, matching: 0)
TraLi38 = Translink.create!(student: Stud5, institute: Inst6, preference_value: 2, matching: 0)
TraLi39 = Translink.create!(student: Stud5, institute: Inst7, preference_value: 7, matching: 0)
TraLi40 = Translink.create!(student: Stud5, institute: Inst8, preference_value: 1, matching: 0)
TraLi41 = Translink.create!(student: Stud6, institute: Inst1, preference_value: 8, matching: 0)
TraLi42 = Translink.create!(student: Stud6, institute: Inst2, preference_value: 1, matching: 0)
TraLi43 = Translink.create!(student: Stud6, institute: Inst3, preference_value: 7, matching: 0)
TraLi44 = Translink.create!(student: Stud6, institute: Inst4, preference_value: 6, matching: 0)
TraLi45 = Translink.create!(student: Stud6, institute: Inst5, preference_value: 6, matching: 0)
TraLi46 = Translink.create!(student: Stud6, institute: Inst6, preference_value: 2, matching: 0)
TraLi47 = Translink.create!(student: Stud6, institute: Inst7, preference_value: 7, matching: 0)
TraLi48 = Translink.create!(student: Stud6, institute: Inst8, preference_value: 1, matching: 0)
TraLi49 = Translink.create!(student: Stud7, institute: Inst1, preference_value: 1, matching: 0)
TraLi50 = Translink.create!(student: Stud7, institute: Inst2, preference_value: 2, matching: 0)
TraLi51 = Translink.create!(student: Stud7, institute: Inst3, preference_value: 6, matching: 0)
TraLi52 = Translink.create!(student: Stud7, institute: Inst4, preference_value: 4, matching: 0)
TraLi53 = Translink.create!(student: Stud7, institute: Inst5, preference_value: 6, matching: 0)
TraLi54 = Translink.create!(student: Stud7, institute: Inst6, preference_value: 2, matching: 0)
TraLi55 = Translink.create!(student: Stud7, institute: Inst7, preference_value: 7, matching: 0)
TraLi56 = Translink.create!(student: Stud7, institute: Inst8, preference_value: 1, matching: 0)
TraLi57 = Translink.create!(student: Stud8, institute: Inst1, preference_value: 4, matching: 0)
TraLi58 = Translink.create!(student: Stud8, institute: Inst2, preference_value: 5, matching: 0)
TraLi59 = Translink.create!(student: Stud8, institute: Inst3, preference_value: 5, matching: 0)
TraLi60 = Translink.create!(student: Stud8, institute: Inst4, preference_value: 1, matching: 0)
TraLi61 = Translink.create!(student: Stud8, institute: Inst5, preference_value: 6, matching: 0)
TraLi62 = Translink.create!(student: Stud8, institute: Inst6, preference_value: 2, matching: 0)
TraLi63 = Translink.create!(student: Stud8, institute: Inst7, preference_value: 7, matching: 0)
TraLi64 = Translink.create!(student: Stud8, institute: Inst8, preference_value: 1, matching: 0)
TraLi65 = Translink.create!(student: Stud9, institute: Inst1, preference_value: 8, matching: 0)
TraLi66 = Translink.create!(student: Stud9, institute: Inst2, preference_value: 1, matching: 0)
TraLi67 = Translink.create!(student: Stud9, institute: Inst3, preference_value: 7, matching: 0)
TraLi68 = Translink.create!(student: Stud9, institute: Inst4, preference_value: 6, matching: 0)
TraLi69 = Translink.create!(student: Stud9, institute: Inst5, preference_value: 6, matching: 0)
TraLi70 = Translink.create!(student: Stud9, institute: Inst6, preference_value: 2, matching: 0)
TraLi71 = Translink.create!(student: Stud9, institute: Inst7, preference_value: 7, matching: 0)
TraLi72 = Translink.create!(student: Stud9, institute: Inst8, preference_value: 1, matching: 0)
TraLi73 = Translink.create!(student: Stud10, institute: Inst1, preference_value: 4, matching: 0)
TraLi74 = Translink.create!(student: Stud10, institute: Inst2, preference_value: 2, matching: 0)
TraLi75 = Translink.create!(student: Stud10, institute: Inst3, preference_value: 6, matching: 0)
TraLi76 = Translink.create!(student: Stud10, institute: Inst4, preference_value: 1, matching: 0)
TraLi77 = Translink.create!(student: Stud10, institute: Inst5, preference_value: 6, matching: 0)
TraLi78 = Translink.create!(student: Stud10, institute: Inst6, preference_value: 2, matching: 0)
TraLi79 = Translink.create!(student: Stud10, institute: Inst7, preference_value: 7, matching: 0)
TraLi80 = Translink.create!(student: Stud10, institute: Inst8, preference_value: 1, matching: 0)
TraLi81 = Translink.create!(student: Stud11, institute: Inst1, preference_value: 1, matching: 0)
TraLi82 = Translink.create!(student: Stud11, institute: Inst2, preference_value: 5, matching: 0)
TraLi83 = Translink.create!(student: Stud11, institute: Inst3, preference_value: 6, matching: 0)
TraLi84 = Translink.create!(student: Stud11, institute: Inst4, preference_value: 7, matching: 0)
TraLi85 = Translink.create!(student: Stud11, institute: Inst5, preference_value: 6, matching: 0)
TraLi86 = Translink.create!(student: Stud11, institute: Inst6, preference_value: 2, matching: 0)
TraLi87 = Translink.create!(student: Stud11, institute: Inst7, preference_value: 7, matching: 0)
TraLi88 = Translink.create!(student: Stud11, institute: Inst8, preference_value: 1, matching: 0)
TraLi89 = Translink.create!(student: Stud12, institute: Inst1, preference_value: 4, matching: 0)
TraLi90 = Translink.create!(student: Stud12, institute: Inst2, preference_value: 1, matching: 0)
TraLi91 = Translink.create!(student: Stud12, institute: Inst3, preference_value: 7, matching: 0)
TraLi92 = Translink.create!(student: Stud12, institute: Inst4, preference_value: 3, matching: 0)
TraLi93 = Translink.create!(student: Stud12, institute: Inst5, preference_value: 6, matching: 0)
TraLi94 = Translink.create!(student: Stud12, institute: Inst6, preference_value: 2, matching: 0)
TraLi95 = Translink.create!(student: Stud12, institute: Inst7, preference_value: 7, matching: 0)
TraLi96 = Translink.create!(student: Stud12, institute: Inst8, preference_value: 1, matching: 0)
TraLi97 = Translink.create!(student: Stud13, institute: Inst1, preference_value: 8, matching: 0)
TraLi98 = Translink.create!(student: Stud13, institute: Inst2, preference_value: 8, matching: 0)
TraLi99 = Translink.create!(student: Stud13, institute: Inst3, preference_value: 7, matching: 0)
TraLi100 = Translink.create!(student: Stud13, institute: Inst4, preference_value: 1, matching: 0)
TraLi101 = Translink.create!(student: Stud13, institute: Inst5, preference_value: 6, matching: 0)
TraLi102 = Translink.create!(student: Stud13, institute: Inst6, preference_value: 2, matching: 0)
TraLi103 = Translink.create!(student: Stud13, institute: Inst7, preference_value: 7, matching: 0)
TraLi104 = Translink.create!(student: Stud13, institute: Inst8, preference_value: 1, matching: 0)
TraLi105 = Translink.create!(student: Stud14, institute: Inst1, preference_value: 2, matching: 0)
TraLi106 = Translink.create!(student: Stud14, institute: Inst2, preference_value: 2, matching: 0)
TraLi107 = Translink.create!(student: Stud14, institute: Inst3, preference_value: 1, matching: 0)
TraLi108 = Translink.create!(student: Stud14, institute: Inst4, preference_value: 7, matching: 0)
TraLi109 = Translink.create!(student: Stud14, institute: Inst5, preference_value: 6, matching: 0)
TraLi110 = Translink.create!(student: Stud14, institute: Inst6, preference_value: 2, matching: 0)
TraLi111 = Translink.create!(student: Stud14, institute: Inst7, preference_value: 7, matching: 0)
TraLi112 = Translink.create!(student: Stud14, institute: Inst8, preference_value: 1, matching: 0)
TraLi113 = Translink.create!(student: Stud15, institute: Inst1, preference_value: 4, matching: 0)
TraLi114 = Translink.create!(student: Stud15, institute: Inst2, preference_value: 1, matching: 0)
TraLi115 = Translink.create!(student: Stud15, institute: Inst3, preference_value: 5, matching: 0)
TraLi116 = Translink.create!(student: Stud15, institute: Inst4, preference_value: 6, matching: 0)
TraLi117 = Translink.create!(student: Stud15, institute: Inst5, preference_value: 6, matching: 0)
TraLi118 = Translink.create!(student: Stud15, institute: Inst6, preference_value: 2, matching: 0)
TraLi119 = Translink.create!(student: Stud15, institute: Inst7, preference_value: 7, matching: 0)
TraLi120 = Translink.create!(student: Stud15, institute: Inst8, preference_value: 1, matching: 0)
TraLi121 = Translink.create!(student: Stud16, institute: Inst1, preference_value: 1, matching: 0)
TraLi122 = Translink.create!(student: Stud16, institute: Inst2, preference_value: 1, matching: 0)
TraLi123 = Translink.create!(student: Stud16, institute: Inst3, preference_value: 1, matching: 0)
TraLi124 = Translink.create!(student: Stud16, institute: Inst4, preference_value: 1, matching: 0)
TraLi125 = Translink.create!(student: Stud16, institute: Inst5, preference_value: 1, matching: 0)
TraLi126 = Translink.create!(student: Stud16, institute: Inst6, preference_value: 1, matching: 0)
TraLi127 = Translink.create!(student: Stud16, institute: Inst7, preference_value: 1, matching: 0)
TraLi128 = Translink.create!(student: Stud16, institute: Inst8, preference_value: 1, matching: 0)
TraLi129 = Translink.create!(student: Stud17, institute: Inst1, preference_value: 1, matching: 0)
TraLi130 = Translink.create!(student: Stud17, institute: Inst2, preference_value: 2, matching: 0)
TraLi131 = Translink.create!(student: Stud17, institute: Inst3, preference_value: 6, matching: 0)
TraLi132 = Translink.create!(student: Stud17, institute: Inst4, preference_value: 5, matching: 0)
TraLi133 = Translink.create!(student: Stud17, institute: Inst5, preference_value: 6, matching: 0)
TraLi134 = Translink.create!(student: Stud17, institute: Inst6, preference_value: 2, matching: 0)
TraLi135 = Translink.create!(student: Stud17, institute: Inst7, preference_value: 7, matching: 0)
TraLi136 = Translink.create!(student: Stud17, institute: Inst8, preference_value: 1, matching: 0)
TraLi137 = Translink.create!(student: Stud18, institute: Inst1, preference_value: 4, matching: 0)
TraLi138 = Translink.create!(student: Stud18, institute: Inst2, preference_value: 9, matching: 0)
TraLi139 = Translink.create!(student: Stud18, institute: Inst3, preference_value: 7, matching: 0)
TraLi140 = Translink.create!(student: Stud18, institute: Inst4, preference_value: 1, matching: 0)
TraLi141 = Translink.create!(student: Stud18, institute: Inst5, preference_value: 6, matching: 0)
TraLi142 = Translink.create!(student: Stud18, institute: Inst6, preference_value: 2, matching: 0)
TraLi143 = Translink.create!(student: Stud18, institute: Inst7, preference_value: 7, matching: 0)
TraLi144 = Translink.create!(student: Stud18, institute: Inst8, preference_value: 1, matching: 0)
TraLi145 = Translink.create!(student: Stud19, institute: Inst1, preference_value: 8, matching: 0)
TraLi146 = Translink.create!(student: Stud19, institute: Inst2, preference_value: 5, matching: 0)
TraLi147 = Translink.create!(student: Stud19, institute: Inst3, preference_value: 1, matching: 0)
TraLi148 = Translink.create!(student: Stud19, institute: Inst4, preference_value: 6, matching: 0)
TraLi149 = Translink.create!(student: Stud19, institute: Inst5, preference_value: 6, matching: 0)
TraLi150 = Translink.create!(student: Stud19, institute: Inst6, preference_value: 2, matching: 0)
TraLi151 = Translink.create!(student: Stud19, institute: Inst7, preference_value: 7, matching: 0)
TraLi152 = Translink.create!(student: Stud19, institute: Inst8, preference_value: 1, matching: 0)
TraLi153 = Translink.create!(student: Stud20, institute: Inst1, preference_value: 6, matching: 0)
TraLi154 = Translink.create!(student: Stud20, institute: Inst2, preference_value: 2, matching: 0)
TraLi155 = Translink.create!(student: Stud20, institute: Inst3, preference_value: 7, matching: 0)
TraLi156 = Translink.create!(student: Stud20, institute: Inst4, preference_value: 1, matching: 0)
TraLi157 = Translink.create!(student: Stud20, institute: Inst5, preference_value: 6, matching: 0)
TraLi158 = Translink.create!(student: Stud20, institute: Inst6, preference_value: 2, matching: 0)
TraLi159 = Translink.create!(student: Stud20, institute: Inst7, preference_value: 7, matching: 0)
TraLi160 = Translink.create!(student: Stud20, institute: Inst8, preference_value: 1, matching: 0)
TraLi161 = Translink.create!(student: Stud21, institute: Inst1, preference_value: 4, matching: 0)
TraLi162 = Translink.create!(student: Stud21, institute: Inst2, preference_value: 4, matching: 0)
TraLi163 = Translink.create!(student: Stud21, institute: Inst3, preference_value: 1, matching: 0)
TraLi164 = Translink.create!(student: Stud21, institute: Inst4, preference_value: 7, matching: 0)
TraLi165 = Translink.create!(student: Stud21, institute: Inst5, preference_value: 4, matching: 0)
TraLi166 = Translink.create!(student: Stud21, institute: Inst6, preference_value: 5, matching: 0)
TraLi167 = Translink.create!(student: Stud21, institute: Inst7, preference_value: 1, matching: 0)
TraLi168 = Translink.create!(student: Stud21, institute: Inst8, preference_value: 3, matching: 0)
TraLi169 = Translink.create!(student: Stud22, institute: Inst1, preference_value: 1, matching: 0)
TraLi170 = Translink.create!(student: Stud22, institute: Inst2, preference_value: 8, matching: 0)
TraLi171 = Translink.create!(student: Stud22, institute: Inst3, preference_value: 7, matching: 0)
TraLi172 = Translink.create!(student: Stud22, institute: Inst4, preference_value: 6, matching: 0)
TraLi173 = Translink.create!(student: Stud22, institute: Inst5, preference_value: 6, matching: 0)
TraLi174 = Translink.create!(student: Stud22, institute: Inst6, preference_value: 2, matching: 0)
TraLi175 = Translink.create!(student: Stud22, institute: Inst7, preference_value: 1, matching: 0)
TraLi176 = Translink.create!(student: Stud22, institute: Inst8, preference_value: 4, matching: 0)
TraLi177 = Translink.create!(student: Stud23, institute: Inst1, preference_value: 4, matching: 0)
TraLi178 = Translink.create!(student: Stud23, institute: Inst2, preference_value: 5, matching: 0)
TraLi179 = Translink.create!(student: Stud23, institute: Inst3, preference_value: 5, matching: 0)
TraLi180 = Translink.create!(student: Stud23, institute: Inst4, preference_value: 1, matching: 0)
TraLi181 = Translink.create!(student: Stud23, institute: Inst5, preference_value: 8, matching: 0)
TraLi182 = Translink.create!(student: Stud23, institute: Inst6, preference_value: 1, matching: 0)
TraLi183 = Translink.create!(student: Stud23, institute: Inst7, preference_value: 7, matching: 0)
TraLi184 = Translink.create!(student: Stud23, institute: Inst8, preference_value: 6, matching: 0)
TraLi185 = Translink.create!(student: Stud24, institute: Inst1, preference_value: 1, matching: 0)
TraLi186 = Translink.create!(student: Stud24, institute: Inst2, preference_value: 2, matching: 0)
TraLi187 = Translink.create!(student: Stud24, institute: Inst3, preference_value: 6, matching: 0)
TraLi188 = Translink.create!(student: Stud24, institute: Inst4, preference_value: 4, matching: 0)
TraLi189 = Translink.create!(student: Stud24, institute: Inst5, preference_value: 4, matching: 0)
TraLi190 = Translink.create!(student: Stud24, institute: Inst6, preference_value: 5, matching: 0)
TraLi191 = Translink.create!(student: Stud24, institute: Inst7, preference_value: 5, matching: 0)
TraLi192 = Translink.create!(student: Stud24, institute: Inst8, preference_value: 1, matching: 0)
TraLi193 = Translink.create!(student: Stud25, institute: Inst1, preference_value: 8, matching: 0)
TraLi194 = Translink.create!(student: Stud25, institute: Inst2, preference_value: 1, matching: 0)
TraLi195 = Translink.create!(student: Stud25, institute: Inst3, preference_value: 7, matching: 0)
TraLi196 = Translink.create!(student: Stud25, institute: Inst4, preference_value: 6, matching: 0)
TraLi197 = Translink.create!(student: Stud25, institute: Inst5, preference_value: 4, matching: 0)
TraLi198 = Translink.create!(student: Stud25, institute: Inst6, preference_value: 2, matching: 0)
TraLi199 = Translink.create!(student: Stud25, institute: Inst7, preference_value: 6, matching: 0)
TraLi200 = Translink.create!(student: Stud25, institute: Inst8, preference_value: 1, matching: 0)
TraLi201 = Translink.create!(student: Stud26, institute: Inst1, preference_value: 1, matching: 0)
TraLi202 = Translink.create!(student: Stud26, institute: Inst2, preference_value: 5, matching: 0)
TraLi203 = Translink.create!(student: Stud26, institute: Inst3, preference_value: 6, matching: 0)
TraLi204 = Translink.create!(student: Stud26, institute: Inst4, preference_value: 7, matching: 0)
TraLi205 = Translink.create!(student: Stud26, institute: Inst5, preference_value: 4, matching: 0)
TraLi206 = Translink.create!(student: Stud26, institute: Inst6, preference_value: 1, matching: 0)
TraLi207 = Translink.create!(student: Stud26, institute: Inst7, preference_value: 7, matching: 0)
TraLi208 = Translink.create!(student: Stud26, institute: Inst8, preference_value: 3, matching: 0)
TraLi209 = Translink.create!(student: Stud27, institute: Inst1, preference_value: 8, matching: 0)
TraLi210 = Translink.create!(student: Stud27, institute: Inst2, preference_value: 8, matching: 0)
TraLi211 = Translink.create!(student: Stud27, institute: Inst3, preference_value: 7, matching: 0)
TraLi212 = Translink.create!(student: Stud27, institute: Inst4, preference_value: 1, matching: 0)
TraLi213 = Translink.create!(student: Stud27, institute: Inst5, preference_value: 2, matching: 0)
TraLi214 = Translink.create!(student: Stud27, institute: Inst6, preference_value: 2, matching: 0)
TraLi215 = Translink.create!(student: Stud27, institute: Inst7, preference_value: 1, matching: 0)
TraLi216 = Translink.create!(student: Stud27, institute: Inst8, preference_value: 7, matching: 0)
TraLi217 = Translink.create!(student: Stud28, institute: Inst1, preference_value: 4, matching: 0)
TraLi218 = Translink.create!(student: Stud28, institute: Inst2, preference_value: 1, matching: 0)
TraLi219 = Translink.create!(student: Stud28, institute: Inst3, preference_value: 5, matching: 0)
TraLi220 = Translink.create!(student: Stud28, institute: Inst4, preference_value: 6, matching: 0)
TraLi221 = Translink.create!(student: Stud28, institute: Inst5, preference_value: 4, matching: 0)
TraLi222 = Translink.create!(student: Stud28, institute: Inst6, preference_value: 8, matching: 0)
TraLi223 = Translink.create!(student: Stud28, institute: Inst7, preference_value: 1, matching: 0)
TraLi224 = Translink.create!(student: Stud28, institute: Inst8, preference_value: 6, matching: 0)
TraLi225 = Translink.create!(student: Stud29, institute: Inst1, preference_value: 1, matching: 0)
TraLi226 = Translink.create!(student: Stud29, institute: Inst2, preference_value: 2, matching: 0)
TraLi227 = Translink.create!(student: Stud29, institute: Inst3, preference_value: 6, matching: 0)
TraLi228 = Translink.create!(student: Stud29, institute: Inst4, preference_value: 5, matching: 0)
TraLi229 = Translink.create!(student: Stud29, institute: Inst5, preference_value: 4, matching: 0)
TraLi230 = Translink.create!(student: Stud29, institute: Inst6, preference_value: 9, matching: 0)
TraLi231 = Translink.create!(student: Stud29, institute: Inst7, preference_value: 7, matching: 0)
TraLi232 = Translink.create!(student: Stud29, institute: Inst8, preference_value: 1, matching: 0)
TraLi233 = Translink.create!(student: Stud30, institute: Inst1, preference_value: 8, matching: 0)
TraLi234 = Translink.create!(student: Stud30, institute: Inst2, preference_value: 5, matching: 0)
TraLi235 = Translink.create!(student: Stud30, institute: Inst3, preference_value: 1, matching: 0)
TraLi236 = Translink.create!(student: Stud30, institute: Inst4, preference_value: 6, matching: 0)
TraLi237 = Translink.create!(student: Stud30, institute: Inst5, preference_value: 6, matching: 0)
TraLi238 = Translink.create!(student: Stud30, institute: Inst6, preference_value: 2, matching: 0)
TraLi239 = Translink.create!(student: Stud30, institute: Inst7, preference_value: 7, matching: 0)
TraLi240 = Translink.create!(student: Stud30, institute: Inst8, preference_value: 1, matching: 0)
TraLi241 = Translink.create!(student: Stud31, institute: Inst1, preference_value: 4, matching: 0)
TraLi242 = Translink.create!(student: Stud31, institute: Inst2, preference_value: 4, matching: 0)
TraLi243 = Translink.create!(student: Stud31, institute: Inst3, preference_value: 1, matching: 0)
TraLi244 = Translink.create!(student: Stud31, institute: Inst4, preference_value: 7, matching: 0)
TraLi245 = Translink.create!(student: Stud31, institute: Inst5, preference_value: 4, matching: 0)
TraLi246 = Translink.create!(student: Stud31, institute: Inst6, preference_value: 5, matching: 0)
TraLi247 = Translink.create!(student: Stud31, institute: Inst7, preference_value: 1, matching: 0)
TraLi248 = Translink.create!(student: Stud31, institute: Inst8, preference_value: 3, matching: 0)
TraLi249 = Translink.create!(student: Stud32, institute: Inst1, preference_value: 1, matching: 0)
TraLi250 = Translink.create!(student: Stud32, institute: Inst2, preference_value: 8, matching: 0)
TraLi251 = Translink.create!(student: Stud32, institute: Inst3, preference_value: 7, matching: 0)
TraLi252 = Translink.create!(student: Stud32, institute: Inst4, preference_value: 6, matching: 0)
TraLi253 = Translink.create!(student: Stud32, institute: Inst5, preference_value: 6, matching: 0)
TraLi254 = Translink.create!(student: Stud32, institute: Inst6, preference_value: 2, matching: 0)
TraLi255 = Translink.create!(student: Stud32, institute: Inst7, preference_value: 1, matching: 0)
TraLi256 = Translink.create!(student: Stud32, institute: Inst8, preference_value: 4, matching: 0)
TraLi257 = Translink.create!(student: Stud33, institute: Inst1, preference_value: 4, matching: 0)
TraLi258 = Translink.create!(student: Stud33, institute: Inst2, preference_value: 5, matching: 0)
TraLi259 = Translink.create!(student: Stud33, institute: Inst3, preference_value: 5, matching: 0)
TraLi260 = Translink.create!(student: Stud33, institute: Inst4, preference_value: 1, matching: 0)
TraLi261 = Translink.create!(student: Stud33, institute: Inst5, preference_value: 8, matching: 0)
TraLi262 = Translink.create!(student: Stud33, institute: Inst6, preference_value: 1, matching: 0)
TraLi263 = Translink.create!(student: Stud33, institute: Inst7, preference_value: 7, matching: 0)
TraLi264 = Translink.create!(student: Stud33, institute: Inst8, preference_value: 6, matching: 0)
TraLi265 = Translink.create!(student: Stud34, institute: Inst1, preference_value: 1, matching: 0)
TraLi266 = Translink.create!(student: Stud34, institute: Inst2, preference_value: 2, matching: 0)
TraLi267 = Translink.create!(student: Stud34, institute: Inst3, preference_value: 6, matching: 0)
TraLi268 = Translink.create!(student: Stud34, institute: Inst4, preference_value: 4, matching: 0)
TraLi269 = Translink.create!(student: Stud34, institute: Inst5, preference_value: 4, matching: 0)
TraLi270 = Translink.create!(student: Stud34, institute: Inst6, preference_value: 5, matching: 0)
TraLi271 = Translink.create!(student: Stud34, institute: Inst7, preference_value: 5, matching: 0)
TraLi272 = Translink.create!(student: Stud34, institute: Inst8, preference_value: 1, matching: 0)
TraLi273 = Translink.create!(student: Stud35, institute: Inst1, preference_value: 8, matching: 0)
TraLi274 = Translink.create!(student: Stud35, institute: Inst2, preference_value: 1, matching: 0)
TraLi275 = Translink.create!(student: Stud35, institute: Inst3, preference_value: 7, matching: 0)
TraLi276 = Translink.create!(student: Stud35, institute: Inst4, preference_value: 6, matching: 0)
TraLi277 = Translink.create!(student: Stud35, institute: Inst5, preference_value: 4, matching: 0)
TraLi278 = Translink.create!(student: Stud35, institute: Inst6, preference_value: 2, matching: 0)
TraLi279 = Translink.create!(student: Stud35, institute: Inst7, preference_value: 6, matching: 0)
TraLi280 = Translink.create!(student: Stud35, institute: Inst8, preference_value: 1, matching: 0)
TraLi281 = Translink.create!(student: Stud36, institute: Inst1, preference_value: 1, matching: 0)
TraLi282 = Translink.create!(student: Stud36, institute: Inst2, preference_value: 1, matching: 0)
TraLi283 = Translink.create!(student: Stud36, institute: Inst3, preference_value: 1, matching: 0)
TraLi284 = Translink.create!(student: Stud36, institute: Inst4, preference_value: 1, matching: 0)
TraLi285 = Translink.create!(student: Stud36, institute: Inst5, preference_value: 1, matching: 0)
TraLi286 = Translink.create!(student: Stud36, institute: Inst6, preference_value: 1, matching: 0)
TraLi287 = Translink.create!(student: Stud36, institute: Inst7, preference_value: 1, matching: 0)
TraLi288 = Translink.create!(student: Stud36, institute: Inst8, preference_value: 1, matching: 0)
TraLi289 = Translink.create!(student: Stud37, institute: Inst1, preference_value: 8, matching: 0)
TraLi290 = Translink.create!(student: Stud37, institute: Inst2, preference_value: 8, matching: 0)
TraLi291 = Translink.create!(student: Stud37, institute: Inst3, preference_value: 7, matching: 0)
TraLi292 = Translink.create!(student: Stud37, institute: Inst4, preference_value: 1, matching: 0)
TraLi293 = Translink.create!(student: Stud37, institute: Inst5, preference_value: 2, matching: 0)
TraLi294 = Translink.create!(student: Stud37, institute: Inst6, preference_value: 2, matching: 0)
TraLi295 = Translink.create!(student: Stud37, institute: Inst7, preference_value: 1, matching: 0)
TraLi296 = Translink.create!(student: Stud37, institute: Inst8, preference_value: 7, matching: 0)
TraLi297 = Translink.create!(student: Stud38, institute: Inst1, preference_value: 4, matching: 0)
TraLi298 = Translink.create!(student: Stud38, institute: Inst2, preference_value: 1, matching: 0)
TraLi299 = Translink.create!(student: Stud38, institute: Inst3, preference_value: 5, matching: 0)
TraLi300 = Translink.create!(student: Stud38, institute: Inst4, preference_value: 6, matching: 0)
TraLi301 = Translink.create!(student: Stud38, institute: Inst5, preference_value: 4, matching: 0)
TraLi302 = Translink.create!(student: Stud38, institute: Inst6, preference_value: 8, matching: 0)
TraLi303 = Translink.create!(student: Stud38, institute: Inst7, preference_value: 1, matching: 0)
TraLi304 = Translink.create!(student: Stud38, institute: Inst8, preference_value: 6, matching: 0)
TraLi305 = Translink.create!(student: Stud39, institute: Inst1, preference_value: 1, matching: 0)
TraLi306 = Translink.create!(student: Stud39, institute: Inst2, preference_value: 2, matching: 0)
TraLi307 = Translink.create!(student: Stud39, institute: Inst3, preference_value: 6, matching: 0)
TraLi308 = Translink.create!(student: Stud39, institute: Inst4, preference_value: 5, matching: 0)
TraLi309 = Translink.create!(student: Stud39, institute: Inst5, preference_value: 4, matching: 0)
TraLi310 = Translink.create!(student: Stud39, institute: Inst6, preference_value: 9, matching: 0)
TraLi311 = Translink.create!(student: Stud39, institute: Inst7, preference_value: 7, matching: 0)
TraLi312 = Translink.create!(student: Stud39, institute: Inst8, preference_value: 1, matching: 0)
TraLi313 = Translink.create!(student: Stud40, institute: Inst1, preference_value: 8, matching: 0)
TraLi314 = Translink.create!(student: Stud40, institute: Inst2, preference_value: 5, matching: 0)
TraLi315 = Translink.create!(student: Stud40, institute: Inst3, preference_value: 1, matching: 0)
TraLi316 = Translink.create!(student: Stud40, institute: Inst4, preference_value: 6, matching: 0)
TraLi317 = Translink.create!(student: Stud40, institute: Inst5, preference_value: 6, matching: 0)
TraLi318 = Translink.create!(student: Stud40, institute: Inst6, preference_value: 2, matching: 0)
TraLi319 = Translink.create!(student: Stud40, institute: Inst7, preference_value: 7, matching: 0)
TraLi320 = Translink.create!(student: Stud40, institute: Inst8, preference_value: 1, matching: 0)
TraLi321 = Translink.create!(student: Stud41, institute: Inst1, preference_value: 4, matching: 0)
TraLi322 = Translink.create!(student: Stud41, institute: Inst2, preference_value: 4, matching: 0)
TraLi323 = Translink.create!(student: Stud41, institute: Inst3, preference_value: 1, matching: 0)
TraLi324 = Translink.create!(student: Stud41, institute: Inst4, preference_value: 7, matching: 0)
TraLi325 = Translink.create!(student: Stud41, institute: Inst5, preference_value: 4, matching: 0)
TraLi326 = Translink.create!(student: Stud41, institute: Inst6, preference_value: 5, matching: 0)
TraLi327 = Translink.create!(student: Stud41, institute: Inst7, preference_value: 1, matching: 0)
TraLi328 = Translink.create!(student: Stud41, institute: Inst8, preference_value: 3, matching: 0)
TraLi329 = Translink.create!(student: Stud42, institute: Inst1, preference_value: 1, matching: 0)
TraLi330 = Translink.create!(student: Stud42, institute: Inst2, preference_value: 8, matching: 0)
TraLi331 = Translink.create!(student: Stud42, institute: Inst3, preference_value: 7, matching: 0)
TraLi332 = Translink.create!(student: Stud42, institute: Inst4, preference_value: 6, matching: 0)
TraLi333 = Translink.create!(student: Stud42, institute: Inst5, preference_value: 6, matching: 0)
TraLi334 = Translink.create!(student: Stud42, institute: Inst6, preference_value: 2, matching: 0)
TraLi335 = Translink.create!(student: Stud42, institute: Inst7, preference_value: 1, matching: 0)
TraLi336 = Translink.create!(student: Stud42, institute: Inst8, preference_value: 4, matching: 0)
TraLi337 = Translink.create!(student: Stud43, institute: Inst1, preference_value: 4, matching: 0)
TraLi338 = Translink.create!(student: Stud43, institute: Inst2, preference_value: 5, matching: 0)
TraLi339 = Translink.create!(student: Stud43, institute: Inst3, preference_value: 5, matching: 0)
TraLi340 = Translink.create!(student: Stud43, institute: Inst4, preference_value: 1, matching: 0)
TraLi341 = Translink.create!(student: Stud43, institute: Inst5, preference_value: 8, matching: 0)
TraLi342 = Translink.create!(student: Stud43, institute: Inst6, preference_value: 1, matching: 0)
TraLi343 = Translink.create!(student: Stud43, institute: Inst7, preference_value: 7, matching: 0)
TraLi344 = Translink.create!(student: Stud43, institute: Inst8, preference_value: 6, matching: 0)
TraLi345 = Translink.create!(student: Stud44, institute: Inst1, preference_value: 1, matching: 0)
TraLi346 = Translink.create!(student: Stud44, institute: Inst2, preference_value: 2, matching: 0)
TraLi347 = Translink.create!(student: Stud44, institute: Inst3, preference_value: 6, matching: 0)
TraLi348 = Translink.create!(student: Stud44, institute: Inst4, preference_value: 4, matching: 0)
TraLi349 = Translink.create!(student: Stud44, institute: Inst5, preference_value: 4, matching: 0)
TraLi350 = Translink.create!(student: Stud44, institute: Inst6, preference_value: 5, matching: 0)
TraLi351 = Translink.create!(student: Stud44, institute: Inst7, preference_value: 5, matching: 0)
TraLi352 = Translink.create!(student: Stud44, institute: Inst8, preference_value: 1, matching: 0)
TraLi353 = Translink.create!(student: Stud45, institute: Inst1, preference_value: 8, matching: 0)
TraLi354 = Translink.create!(student: Stud45, institute: Inst2, preference_value: 1, matching: 0)
TraLi355 = Translink.create!(student: Stud45, institute: Inst3, preference_value: 7, matching: 0)
TraLi356 = Translink.create!(student: Stud45, institute: Inst4, preference_value: 6, matching: 0)
TraLi357 = Translink.create!(student: Stud45, institute: Inst5, preference_value: 4, matching: 0)
TraLi358 = Translink.create!(student: Stud45, institute: Inst6, preference_value: 2, matching: 0)
TraLi359 = Translink.create!(student: Stud45, institute: Inst7, preference_value: 6, matching: 0)
TraLi360 = Translink.create!(student: Stud45, institute: Inst8, preference_value: 1, matching: 0)
TraLi361 = Translink.create!(student: Stud46, institute: Inst1, preference_value: 1, matching: 0)
TraLi362 = Translink.create!(student: Stud46, institute: Inst2, preference_value: 5, matching: 0)
TraLi363 = Translink.create!(student: Stud46, institute: Inst3, preference_value: 6, matching: 0)
TraLi364 = Translink.create!(student: Stud46, institute: Inst4, preference_value: 7, matching: 0)
TraLi365 = Translink.create!(student: Stud46, institute: Inst5, preference_value: 4, matching: 0)
TraLi366 = Translink.create!(student: Stud46, institute: Inst6, preference_value: 1, matching: 0)
TraLi367 = Translink.create!(student: Stud46, institute: Inst7, preference_value: 7, matching: 0)
TraLi368 = Translink.create!(student: Stud46, institute: Inst8, preference_value: 3, matching: 0)
TraLi369 = Translink.create!(student: Stud47, institute: Inst1, preference_value: 8, matching: 0)
TraLi370 = Translink.create!(student: Stud47, institute: Inst2, preference_value: 8, matching: 0)
TraLi371 = Translink.create!(student: Stud47, institute: Inst3, preference_value: 7, matching: 0)
TraLi372 = Translink.create!(student: Stud47, institute: Inst4, preference_value: 1, matching: 0)
TraLi373 = Translink.create!(student: Stud47, institute: Inst5, preference_value: 2, matching: 0)
TraLi374 = Translink.create!(student: Stud47, institute: Inst6, preference_value: 2, matching: 0)
TraLi375 = Translink.create!(student: Stud47, institute: Inst7, preference_value: 1, matching: 0)
TraLi376 = Translink.create!(student: Stud47, institute: Inst8, preference_value: 7, matching: 0)
TraLi377 = Translink.create!(student: Stud48, institute: Inst1, preference_value: 4, matching: 0)
TraLi378 = Translink.create!(student: Stud48, institute: Inst2, preference_value: 1, matching: 0)
TraLi379 = Translink.create!(student: Stud48, institute: Inst3, preference_value: 5, matching: 0)
TraLi380 = Translink.create!(student: Stud48, institute: Inst4, preference_value: 6, matching: 0)
TraLi381 = Translink.create!(student: Stud48, institute: Inst5, preference_value: 4, matching: 0)
TraLi382 = Translink.create!(student: Stud48, institute: Inst6, preference_value: 8, matching: 0)
TraLi383 = Translink.create!(student: Stud48, institute: Inst7, preference_value: 1, matching: 0)
TraLi384 = Translink.create!(student: Stud48, institute: Inst8, preference_value: 6, matching: 0)
TraLi385 = Translink.create!(student: Stud49, institute: Inst1, preference_value: 1, matching: 0)
TraLi386 = Translink.create!(student: Stud49, institute: Inst2, preference_value: 2, matching: 0)
TraLi387 = Translink.create!(student: Stud49, institute: Inst3, preference_value: 6, matching: 0)
TraLi388 = Translink.create!(student: Stud49, institute: Inst4, preference_value: 5, matching: 0)
TraLi389 = Translink.create!(student: Stud49, institute: Inst5, preference_value: 4, matching: 0)
TraLi390 = Translink.create!(student: Stud49, institute: Inst6, preference_value: 9, matching: 0)
TraLi391 = Translink.create!(student: Stud49, institute: Inst7, preference_value: 7, matching: 0)
TraLi392 = Translink.create!(student: Stud49, institute: Inst8, preference_value: 1, matching: 0)
TraLi393 = Translink.create!(student: Stud50, institute: Inst1, preference_value: 8, matching: 0)
TraLi394 = Translink.create!(student: Stud50, institute: Inst2, preference_value: 5, matching: 0)
TraLi395 = Translink.create!(student: Stud50, institute: Inst3, preference_value: 1, matching: 0)
TraLi396 = Translink.create!(student: Stud50, institute: Inst4, preference_value: 6, matching: 0)
TraLi397 = Translink.create!(student: Stud50, institute: Inst5, preference_value: 6, matching: 0)
TraLi398 = Translink.create!(student: Stud50, institute: Inst6, preference_value: 2, matching: 0)
TraLi399 = Translink.create!(student: Stud50, institute: Inst7, preference_value: 7, matching: 0)
TraLi400 = Translink.create!(student: Stud50, institute: Inst8, preference_value: 1, matching: 0)

GamsPath.create!(gams_path_url: "C:\\GAMS\\win64\\24.5\\gams")
