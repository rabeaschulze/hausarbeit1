option optcr = 0 ;

Sets
         i               Menge der Studenten
         j               Menge der Institute
         l               Links fuer Zuordnung
         LI(l,i), LJ(l,j) ;

Parameter
         p(l)            Praeferenzwert von Student i zu Institut j
         oc(j)           prozentuale Ueberschreitung der Betreuungskapazitaet von Institut j
         anzprof(j)      Professorenanzahl
         anzma(j)        Mitarbeiteranzahl
         gesanz          Gesamtanzahl der Studenten
         gesprof         Gesamtanzahl der Professoren
         gesma           Gesamtanzahl der Mitarbeiter
         dl              Durchschnittsleid der Studenten ;

Binary Variable
         x(l)            1 wenn Student i Institut j zugewiesen bekommt | 0 sonst ;

Positive Variables
         k(j)            Betreuungskapazitaet von Institut j
         bk(j)           gesamte Betreuungskapazitaet von Institut j
         prof(j)         Gewichtungsfaktor Anzahl der Professoren von Institut j
         ma(j)           Gewichtungsfaktor Anzahl der Mitarbeiter von Institut j ;

Free Variable
         Z               Zielfunktionswert ;

Equations
         ZF              Zielfunktion
         ZuO(i)          jeder Student wird einem Institut zugeordnet
         Ins(j)          jedes Institut darf mehrere Studenten haben
         GProf(j)        gewichtete Professoren
         GMa(j)          gewichtete Mitarbeiter
         Betr(j)         Betreuungskapazitaet
         Ges(j)          Gesamtkapazitaet
         Kap(j)          Kapazitaetsbedingung ;

ZF..             sum(l, x(l) * p(l)) =e= Z ;

ZuO(i)..         sum(l$LI(l,i), x(l)) =e= 1 ;

Ins(j)..         sum(l$LJ(l,j), x(l)) =g= 1 ;

GProf(j)..       prof(j) =e= anzprof(j) / 11 ;

GMa(j)..         ma(j) =e= anzma(j) / 46 ;

Betr(j)..        k(j) =e= 50 * (prof(j) + ma(j)) ;

Ges(j)..         bk(j) =e= k(j) * (1 + oc(j)) ;

Kap(j)..         bk(j) =g= sum(l$LJ(l,j), x(l)) ;

$include Include.inc

Model Zuordnung / all / ;

Solve Zuordnung minimizing Z using mip ;

dl = Z.l / 50 ;

Equation
         Fair(i)         Fairness ;

Fair(i)..        sum(l$LI(l,i), x(l) * p(l)) =l= ceil(dl) ;

Model Durchschnittsleid / all / ;

Solve Durchschnittsleid minimizing Z using mip ;

Display x.l ;

file outputfile1 / 'Matching.txt'/;
put outputfile1;


putclose outputfile1;



file outputfile2 / 'Zielfunktionswert.txt'/;
put outputfile2;


put 'Zielfunktionswert:  ',z.l /
put '**************************'

putclose outputfile2;
