class GamsPath < ApplicationRecord
  validates :gams_path_url, presence: true
end
