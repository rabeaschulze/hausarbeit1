class Student < ApplicationRecord
    belongs_to :institute
    has_many :translinks, :dependent => :destroy
    accepts_nested_attributes_for :translinks
end
