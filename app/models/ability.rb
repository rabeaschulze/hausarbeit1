class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user || User.new # in case of a new user
    @user.roles.each { |role| send(role.name.downcase) }

    if @user.roles.size == 0
      can [:index, :new, :create], User
    end
  end

  def student
    can [:index, :edit, :show, :be, :update, :destroy], Student
    can [:index, :show, :edit, :destroy, :update], User
  end

  def institute
    can [:index, :edit, :show, :be, :update, :destroy], Institute
    can [:index, :show, :edit, :destroy, :update], User
  end

  def admin
    can :manage, :all
  end

  def guest
    can [:index, :new, :create, :edit, :update ], User
  end

end
