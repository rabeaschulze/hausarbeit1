json.extract! translink, :id, :student_id, :institute_id, :preference_value, :matching, :created_at, :updated_at
json.url translink_url(translink, format: :json)
