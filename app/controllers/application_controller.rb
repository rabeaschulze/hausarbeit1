class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper


  rescue_from CanCan::AccessDenied do |exception|
    flash[:error] = "You have been denied access."
    redirect_to matching_start_url
  end

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Please log in."
      redirect_to login_url
    end
  end

end
