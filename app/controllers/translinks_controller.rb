class TranslinksController < ApplicationController
  load_and_authorize_resource
  before_action :set_translink, only: [:show, :edit, :update, :destroy]

  # GET /translinks
  # GET /translinks.json
  def index
    @translinks = Translink.all

    respond_to do |format|
      format.html
      format.xls
    end
  end

  # GET /translinks/1
  # GET /translinks/1.json
  def show
  end

  # GET /translinks/new
  def new
    @translink = Translink.new
  end

  # GET /translinks/1/edit
  def edit
  end

  # POST /translinks
  # POST /translinks.json
  def create
    @translink = Translink.new(translink_params)

    respond_to do |format|
      if @translink.save
        format.html { redirect_to @translink, notice: 'Translink was successfully created.' }
        format.json { render :show, status: :created, location: @translink }
      else
        format.html { render :new }
        format.json { render json: @translink.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /translinks/1
  # PATCH/PUT /translinks/1.json
  def update
    respond_to do |format|
      if @translink.update(translink_params)
        format.html { redirect_to @translink, notice: 'Translink was successfully updated.' }
        format.json { render :show, status: :ok, location: @translink }
      else
        format.html { render :edit }
        format.json { render json: @translink.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /translinks/1
  # DELETE /translinks/1.json
  def destroy
    @translink.destroy
    respond_to do |format|
      format.html { redirect_to translinks_url, notice: 'Translink was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def delete_matching

    if File.exist?("Matching.txt")
      File.delete("Matching.txt")
    end
    if File.exist?("Zielfunktionswert.txt")
      File.delete("Zielfunktionswert.txt")
    end

    @translinks = Translink.all
    @translinks.each { |li|
  #      li.matching=0.0
      li.matching=nil
      li.save
    }

    @objective_function_value=nil

    render :template => "translinks/index"

  end

  def optimize
    # Delete old include file
    if File.exist?("Include.inc")
      File.delete("Include.inc")
    end
    # New include file
    f=File.new("Include.inc", "w")

    # Sets
    printf(f, "set i / \n")
    @students = Student.all
    @students.each { |stud| printf(f, "i" + stud.id.to_s + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "j / \n")
    @institutes = Institute.all
    @institutes.each { |inst| printf(f, "j" + inst.id.to_s + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "l / \n")
    @translinks = Translink.all
    @translinks.each { |li| printf(f, "l" + li.id.to_s + "\n") }
    printf(f, "/;" + "\n\n")

    printf(f, "LI(l,i) = no;\n")
    printf(f, "LJ(l,j) = no;\n\n")

    @translinks.each do |li|
      printf(f, "LI( 'l" + li.id.to_s + "', 'i" + li.student_id.to_s + "') = yes;\n")
      printf(f, "LJ( 'l" + li.id.to_s + "', 'j" + li.institute_id.to_s + "') = yes;\n\n")
    end

    printf(f, "\n\n")

    printf(f, "Parameter\n  oc(j) /\n")

    @institutes.each { |so| printf(f, "j" + so.id.to_s + "  " + so.excess_capacity.to_s + "\n") }
    printf(f, "/" + "\n\n")


    printf(f, "\nanzprof(j) /\n")

    @institutes.each { |si| printf(f, "j" + si.id.to_s + "  " + si.number_of_professors.to_s + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "\nanzma(j) /\n")

    @institutes.each { |si| printf(f, "j" + si.id.to_s + "  " + si.number_of_employees.to_s + "\n") }
    printf(f, "/" + "\n\n")

    printf(f, "\np(l) /\n")

    @translinks.each { |li| printf(f, "l" + li.id.to_s + "  " + li.preference_value.to_s + "\n") }
    printf(f, "/" + "\n\n")

#    printf(f, "/t gesanz \t\t / " + so.gesanz.to_s + "\n")

#    printf(f, "/t gesprof \t\t / " + so.gesprof.to_s + "\n")

#    printf(f, "/t gesma \t\t / " + so.gesma.to_s + "\n")

    printf(f, ";\n")
    f.close


    if File.exist?("Matching.txt")
      File.delete("Matching.txt")
    end

   system "gams Bachelorarbeit1"

    flash.now[:success] = "Matching has been launched!"
    render 'static_pages/matching_start'

  end


  def show_matching_and_ofv

    if File.exist?("Matching.txt")

      fi=File.open("Matching.txt", "r")
      fi.each { |line| # printf(f,line)
        sa=line.split(";")
        sa0=sa[0].delete "l "
        sa3=sa[3].delete " \n"
        al=Translink.find_by_id(sa0)
        al.matching=sa3
        al.save

      }
      fi.close
      @translinks = Translink.all
#      render :template => "translinks/index"
    else
      flash.now[:error] = "Matching isn't yet computed!"
    end

    if File.exist?("Zielfunktionswert.txt")
      fi=File.open("Zielfunktionswert.txt", "r")
      line=fi.readline
      fi.close
      sa=line.split(" ")
      @objective_function_value=sa[1]
    else
      @objective_function_value=nil
     flash.now[:not_available] = "Matching isn't yet computed!"
    end

  #render
      @translinks = Translink.all
      render :template => "translinks/index"

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_translink
      @translink = Translink.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def translink_params
      params.require(:translink).permit(:student_id, :institute_id, :preference_value, :matching)
    end
end
