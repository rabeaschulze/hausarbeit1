Rails.application.routes.draw do
  resources :gams_paths
  get 'gams_path/edit'

  get 'gams_path/update'

  resources :translinks
  resources :institutes
  resources :students

  root   'static_pages#home'
  get    '/about', to: 'static_pages#about'
  get    '/contact', to: 'contacts#new'
  get    '/signup', to: 'users#new'
  get    '/login', to: 'sessions#new'
  get    'matching_start', to: 'static_pages#matching_start'
  post   '/login', to: 'sessions#create'
  post   '/signup', to: 'users#new'
  delete '/logout', to: 'sessions#destroy'
  match '/contacts', to: 'contacts#new', via: 'get'
  resources :contacts, only: [:new, :create]
  resources :users
  get 'show_matching_and_ofv', to: 'translinks#show_matching_and_ofv'
  post 'translinks/delete_matching', :to => 'translinks#delete_matching'
  post 'translinks/optimize', :to => 'translinks#optimize'
  post 'translinks/show_matching_and_ofv', :to => 'translinks#show_matching_and_ofv'
end
