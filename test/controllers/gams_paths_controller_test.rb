require 'test_helper'

class GamsPathsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @gams_path = gams_paths(:one)
  end

  test "should get index" do
    get gams_paths_url
    assert_response :success
  end

  test "should get new" do
    get new_gams_path_url
    assert_response :success
  end

#  test "should create gams_path" do
#    assert_difference('GamsPath.count') do
#      post gams_paths_url, params: { gams_path: { gams_path_url: @gams_path.gams_path_url } }
#    end

#    assert_redirected_to gams_path_url(GamsPath.last)
#  end

  test "should show gams_path" do
    get gams_path_url(@gams_path)
    assert_response :success
  end

  test "should get edit" do
    get edit_gams_path_url(@gams_path)
    assert_response :success
  end

  test "should update gams_path" do
    patch gams_path_url(@gams_path), params: { gams_path: { gams_path_url: @gams_path.gams_path_url } }
    assert_redirected_to gams_path_url(@gams_path)
  end
end

#  test "should destroy gams_path" do
#    assert_difference('GamsPath.count', -1) do
#      delete gams_path_url(@gams_path)
#    end
#
#    assert_redirected_to gams_paths_url
#  end
